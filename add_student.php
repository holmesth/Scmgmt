<?php 
    session_start(); 
    require_once 'config/db.php';
    // if (!isset($_SESSION['admin_login'])) {
    //     $_SESSION['error'] = 'กรุณาเข้าสู่ระบบ!';
    //     header('location: index.php');
    // }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">
    <link rel="stylesheet" href="css/dashboard.css">
    <link rel="stylesheet" href="css/input.css">
</head>
<body>
    <?php 
        if (isset($_SESSION['admin_login'])) {
            $user_id = $_SESSION['admin_login'];
            $stmt = $conn->query("SELECT * FROM admins WHERE user_id = $user_id");
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            //แสดงชื่อผู้ใช้

        }
    ?>
    <div class="head">
        <div class="hi">
            <div class="img">
                <img src="img/user.png" alt="">
            </div>
            <div class="hello">
                <h3>สวัสดีคุณ</h3>
                <h3>นักเรียน เรียนดี</h4>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="logo">
            <h1>โรงเรียนของหนู</h1>
        </div>

        <ul>
            <li><a href="admin.php"><i id="ic" class="fa-solid fa-house"></i>หน้าหลัก</a></li>
            <li><a href="mgmt_admin.php"><i id="ic"class="fa-solid fa-user"></i>จัดการผู้ใช้งานระบบ</a></li>
            <li><a href="mgmt_sub.php"><i id="ic"class="fa-solid fa-book"></i>จัดการรายวิชา</a></li>
            <li><a href="mgmt_score.php"><i id="ic"class="fa-solid fa-font"></i>จัดการคะแนน</a></li>
            <li><a href="personal_info.php"><i id="ic"class="fa-solid fa-lock"></i>ข้อมูลส่วนตัว</a></li>
            <li class="logout" ><a href="logout.php"><i id="ic"class="fa-solid fa-right-from-bracket"></i>ออกจากระบบ</a></li>
        </ul>
    </div>
    <div class="title">
        <i id="tt"class="fa-solid fa-graduation-cap"></i>
        <p>เพิ่มข้อมูลนักเรียน</p>
    </div>
    <div class="content">
        
        <div class="input">
            <form action="PHP_insert_admin.php" enctype="multipart/form-data" method="post">
                <div class="warn">
                    <?php if(isset($_SESSION['warning'])) { ?>
                        <div class="warning">
                            <?php 
                               echo $_SESSION['warning'];
                               unset($_SESSION['warning']);
                            ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="user-detail">
                    <div class="img-input">
                        <figure>
                            <img  alt="" width="120" height="120" id="chosen-image" >
                            <figcaption id="file-name">image.jpg</figcaption>
                        </figure>
                        <input type="file" accept="image/*" id="upload-button" name="file">
                        <label for="upload-button" id="btnimg">
                            <i id="ic" class="fa-solid fa-pen"></i> &nbsp;เปลี่ยนรูปภาพ
                        </label>
                    </div>
                    <div class="text-input">
                        <div class="taxt-input-box">
                            <span class="details">ชื่อ-นามสกุล</span>
                            <input type="text" name="fname" placeholder="กรอกชื่อและนามสกุล" >
                        </div>
                        <div class="taxt-input-box">
                            <span class="details">อีเมล</span>
                            <input type="email" name="email" placeholder="กรอกอีเมล" >
                        </div>
                        <div class="taxt-input-box">
                            <span class="details">เบอร์โทร</span>
                            <input type="text" name="tell" placeholder="กรอกเบอร์โทรติดต่อ" >
                        </div>
                        <div class="taxt-input-box">
                            <span class="details">ตำแหน่ง</span>
                            <input type="text" name="position" placeholder="กรอกตำแหน่งงาน" >
                        </div>
                        <div class="taxt-input-box">
                            <span class="details">รหัสผ่าน</span>
                            <input type="password"  name="password" placeholder="กรกอรหัสผ่าน" >
                        </div>
                        <div class="taxt-input-box">
                            <span class="details">ยืนยันรหัสผ่าน</span>
                            <input type="password"  name="cfpassword" placeholder="กรอกรหัสผ่านยืนยัน" >
                        </div>
                    </div>
                </div>
                <div class="button-input">
                    <button id ="btnsave" name="btnsave" type="submit" class="button-27">บันทึก</button>
                    <button id ="btncancel" type="button" class="button-27" onclick="location.href='mgmt_admin.php'">ยกเลิก</button>
                </div>
            </form> 
        </div>
    </div>
</body>
</html>