<?php 

    session_start();
    require_once 'config/db.php';
    if (!isset($_SESSION['student_login'])) {
        $_SESSION['error'] = 'กรุณาเข้าสู่ระบบ!';
        header('location: index.php');
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>หน้าแรก</title>
</head>
<body>
    <a href="logout.php" class="btn btn-danger">Logout</a>
</body>
</html>