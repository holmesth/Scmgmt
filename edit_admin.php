<?php 
    session_start(); 
    require_once 'config/db.php';
    // if (!isset($_SESSION['admin_login'])) {
    //     $_SESSION['error'] = 'กรุณาเข้าสู่ระบบ!';
    //     header('location: index.php');
    // }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">
    <link rel="stylesheet" href="css/dashboard.css">
    <link rel="stylesheet" href="css/add.css">
</head>
<body>
    <?php 
        if (isset($_SESSION['admin_login'])) {
            $user_id = $_SESSION['admin_login'];
            $stmt = $conn->query("SELECT * FROM admins WHERE user_id = $user_id");
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            //แสดงชื่อผู้ใช้

        }
    ?>
    <div class="head">
        <div class="hi">
            <div class="img">
                <img src="img/user.png" alt="">
            </div>
            <div class="hello">
                <h3>สวัสดีคุณ</h3>
                <h3>นักเรียน เรียนดี</h4>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="logo">
            <h1>โรงเรียนของหนู</h1>
        </div>

        <ul>
            <li><a href="admin.php"><i id="ic" class="fa-solid fa-house"></i>หน้าหลัก</a></li>
            <li><a href="mgmt_admin.php"><i id="ic"class="fa-solid fa-user"></i>จัดการผู้ใช้งานระบบ</a></li>
            <li><a href="mgmt_sub.php"><i id="ic"class="fa-solid fa-book"></i>จัดการรายวิชา</a></li>
            <li><a href="mgmt_score.php"><i id="ic"class="fa-solid fa-font"></i>จัดการคะแนน</a></li>
            <li><a href="personal_info.php"><i id="ic"class="fa-solid fa-lock"></i>ข้อมูลส่วนตัว</a></li>
            <li class="logout" ><a href="logout.php"><i id="ic"class="fa-solid fa-right-from-bracket"></i>ออกจากระบบ</a></li>
        </ul>
    </div>
    <div class="title">
        <i id="tt"class="fa-solid fa-wrench"></i>
        <p>แก้ไขข้อมูลแอดมิน</p>
    </div>
    <div class="content">
    <?php 
        if(isset($_GET['id'])){
            $id = $_GET['id'];
        }else{
            $id = $_SESSION['id'];
        }
        
        $stmt = $conn->query("SELECT * FROM admins ,users WHERE users.user_id = $id AND admins.user_id =$id");
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $image  ="img/".$data['img'];
        
    ?>
    <div class="input">
            <form action="PHP_update_admin.php"  method="post">
                <div class="warn">
                    <?php if(isset($_SESSION['warning'])) { ?>
                        <div class="warning">
                            <?php 
                               echo $_SESSION['warning'];
                               unset($_SESSION['warning']);
                            ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="user-detail">
                    <div class="img-input">
                        <figure>
                            <div class="show-img">
                                <img  src="<?= $image ?>" width="150" height="150" id="chosen-image" >
                            </div>
                            <div class="show-name-img">
                                <figcaption id="file-name"></figcaption>
                            </div>
                        </figure>
                        <div class="real-button-add-img">
                            <input type="file" accept="image/*" id="upload-button" name="file">
                        </div>
                        
                        <div class="button-add-img" >
                            <label for="upload-button" id="btnimg" ">
                                <i id="ic" class="fa-solid fa-pen"></i> &nbsp;เปลี่ยนรูปภาพ
                            </label>
                        </div>
                    </div>
                    <div class="text-input">
                        <input type="hidden" value="<?php echo $id; ?>"  name="id" >
                        <input type="hidden" value="<?php echo $data['img']; ?>" name="old-img" >
                        <input type="hidden" value="<?= $data['password'] ?>" name="old-password">
                        <div class="taxt-input-box">
                            <span class="details">ชื่อ-นามสกุล</span>
                            <input type="text" name="fname" value="<?= $data['firstname'] . ' ' . $data['lastname']; ?>">
                        </div>
                        <div class="taxt-input-box">
                            <span class="details">อีเมล</span>
                            <input type="email" name="email" value="<?= $data['email'] ?>">
                        </div>
                        <div class="taxt-input-box">
                            <span class="details">เบอร์โทร</span>
                            <input type="text" name="tell" value="<?= $data['tel_number'] ?>">
                        </div>
                        <div class="taxt-input-box">
                            <span class="details">ตำแหน่ง</span>
                            <input type="text" name="position" value="<?= $data['position'] ?>">
                        </div>
                        <div class="taxt-input-box">
                            <span class="details">รหัสผ่าน</span>
                            <input type="password"  name="password" value="unknow621602">
                        </div>
                        <div class="taxt-input-box">
                            <span class="details">ยืนยันรหัสผ่าน</span>
                            <input type="password"  name="cfpassword" value="unknow621602">
                        </div>
                    </div>
                </div>
                <div class="button-input">
                    <button id ="btnsave" name="btnUptdete" type="submit" class="button-27">บันทึก</button>
                    <button id ="btncancel" type="button" class="button-27" onclick="location.href='mgmt_admin.php'">ยกเลิก</button>
                </div>
            </form> 
        </div>
    </div>
    <script>
        let uploadButton = document.getElementById("upload-button");
        let chosenImage = document.getElementById("chosen-image");
        let fileName = document.getElementById("file-name");

        uploadButton.onchange = () =>{
            let reader = new FileReader();
            reader.readAsDataURL(uploadButton.files[0]);
            reader.onload = () => {
                chosenImage.setAttribute("src",reader.result);
            }
            fileName.textContent = uploadButton.files[0].name;
        }
    </script>
</body>
</html>