<script src="https://code.jquery.com/jquery-3.6.3.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<?php 

    session_start();

    require_once 'config/db.php';

    if (isset($_POST['btnUptdete'])) {
        $id = $_POST['id'];
        $fullname = $_POST['fname'];
        $email = $_POST['email'];
        $tel_num = $_POST['tell'];
        $position = $_POST['position'];
        $password = $_POST['password'];
        $cfpassword = $_POST['cfpassword'];
        $old_password = $_POST['old-password'];
        $old_img = $_POST['old-img'];
        isset( $_FILES['file']['name'] ) ? $upload = $_FILES['file']['name'] : $upload = "";
        $full_name = explode(" ", $fullname);
        isset( $full_name[0] ) ? $firstname = $full_name[0] : $firstname = "";
        isset( $full_name[1] ) ? $lastname = $full_name[1] : $lastname = "";
        
        
        

        if ($upload != '') {
            $est = explode(".", $_FILES["file"]["name"]);
            $fileName = md5(uniqid()).".{$est[1]}";
            $targetDir = 'img/'.$fileNew;
            move_uploaded_file($_FILES['file']['tmp_name'], $targetFilePath);
            
        } else {
            $fileNew = $old_img;
        }

        if (empty($fullname)){
            $_SESSION['warning'] = 'กรุณากรอกชื่อ-นามสกุล';
            $_SESSION['id']= $id;
            header("location: edit_admin.php");
        }else if (empty($email)) {
            $_SESSION['warning'] = 'กรุณากรอกอีเมล';
            $_SESSION['id']= $id;
            header("location: edit_admin.php");
        } else if (empty($tel_num)) {
            $_SESSION['warning'] = 'กรุณากรอกเบอร์โทร';
            $_SESSION['id']= $id;
            header("location: edit_admin.php");
        }else if (empty($position)) {
            $_SESSION['warning'] = 'กรุณากรอกตำแหน่ง';
            $_SESSION['id']= $id;
            header("location: edit_admin.php");
        }else if (empty($password)) {
            $_SESSION['warning'] = 'กรุณากรอกรหัสผ่าน';
            $_SESSION['id']= $id;
            header("location: edit_admin.php");
        }else if (empty($cfpassword)) {
            $_SESSION['warning'] = 'กรุณากรอกยืนยันรหัสผ่าน';
            $_SESSION['id']= $id;
            header("location: edit_admin.php");
        }else if ($cfpassword != $password) {
            $_SESSION['warning'] = 'ยืนยันรหัสผ่านไม่ถูกต้อง';
            $_SESSION['id']= $id;
            header("location: edit_admin.php");
        }else if (empty($lastname)) {
            $_SESSION['warning'] = 'กรอกชื่อไม่ถูกต้อง';
            $_SESSION['id']= $id;
            header("location: edit_admin.php");
        }else{
            if($password == 'unknow621602'){
                $passwordHash = $old_password;
            }else{
                $passwordHash = password_hash($password, PASSWORD_DEFAULT);
            }

            $sql_user = $conn->prepare("UPDATE users SET email = :email, password = :password  WHERE user_id = :id");
            $sql_user->bindParam(":id", $id);
            $sql_user->bindParam(":email", $email);
            $sql_user->bindParam(":password", $passwordHash);
            $sql_user->execute();

            $sql_admin = $conn->prepare("UPDATE admins SET firstname = :firstname, lastname = :lastname, position = :position, tel_number = :tel_number, img = :img WHERE user_id = :id");
            $sql_admin->bindParam(":id", $id);
            $sql_admin->bindParam(":firstname", $firstname);
            $sql_admin->bindParam(":lastname", $lastname);
            $sql_admin->bindParam(":position", $position);
            $sql_admin->bindParam(":tel_number", $tel_num);
            $sql_admin->bindParam(":img", $fileNew);
            $sql_admin->execute();

            if ($sql_admin && $sql_user) {
                header("location: mgmt_admin.php");
            } else {
                $_SESSION['error'] = "เกิดข้อผิดพลาด";
                header("location: edit_admin.php");
            }
        }
        
        
    }

?>