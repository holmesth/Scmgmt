<?php 
    session_start(); 
    require_once 'config/db.php';
    // if (!isset($_SESSION['admin_login'])) {
    //     $_SESSION['error'] = 'กรุณาเข้าสู่ระบบ!';
    //     header('location: index.php');
    // }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">
    <link rel="stylesheet" href="css/dashboard.css">
    <link rel="stylesheet" href="css/input.css">
</head>
<body>
    <?php 
        if (isset($_SESSION['admin_login'])) {
            $user_id = $_SESSION['admin_login'];
            $stmt = $conn->query("SELECT * FROM admins WHERE user_id = $user_id");
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            //แสดงชื่อผู้ใช้

        }
    ?>
    <div class="head">
        <div class="hi">
            <div class="img">
                <img src="img/user.png" alt="">
            </div>
            <div class="hello">
                <h3>สวัสดีคุณ</h3>
                <h3>นักเรียน เรียนดี</h4>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="logo">
            <h1>โรงเรียนของหนู</h1>
        </div>

        <ul>
            <li><a href="admin.php"><i id="ic" class="fa-solid fa-house"></i>หน้าหลัก</a></li>
            <li><a href="mgmt_admin.php"><i id="ic"class="fa-solid fa-user"></i>จัดการผู้ใช้งานระบบ</a></li>
            <li><a href="mgmt_sub.php"><i id="ic"class="fa-solid fa-book"></i>จัดการรายวิชา</a></li>
            <li><a href="mgmt_score.php"><i id="ic"class="fa-solid fa-font"></i>จัดการคะแนน</a></li>
            <li><a href="personal_info.php"><i id="ic"class="fa-solid fa-lock"></i>ข้อมูลส่วนตัว</a></li>
            <li class="logout" ><a href="logout.php"><i id="ic"class="fa-solid fa-right-from-bracket"></i>ออกจากระบบ</a></li>
        </ul>
    </div>
    <div class="title">
        <i id="tt"class="fa-solid fa-book"></i>
        <p>เพิ่มข้อมูลรายวิชา</p>
    </div>
    <div class="content">
        
        <div class="input">
           <form action="test.php">
           <div class="tterm">
            <label for="cars">ภาคเรียนที:</label><br>
                <select name="term" id="term">
                    <option value=""></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                </select>
            </div>
            <div class="tyear">
                <label for="year">ปีการศึกษา</label><br>
                <input type="text"  name="year">
            </div>
            <div class="tclass">
                <label for="class">ชั้นเรียน</label><br>
                <select name="class" id="class" >
                    <option value=""></option>
                    <option value="1/1">ม.1/1</option>
                    <option value="1/2">ม.1/2</option>
                    <option value="2/1">ม.2/1</option>
                    <option value="2/2">ม.2/2</option>
                </select>
            </div>
            <div class="front-sub">
                <label for="idsub">รหัสวิชา</label><br>
                <input type="text"  name="sub"><br>
                <label for="teacher">อาจารย์ผู้สอน</label><br>
                <select name="teacher" id="teacher" >
                    <option ></option>
                    <option value="กานต์ พงศ์ธนา">กานต์ พงศ์ธนา</option>
                    <option selected ="ชลธี จรัสโสภณ" value="บัลลังค์ อุดมเอก">บัลลังค์ อุดมเอก</option>
                    <option value="ชลธี จรัสโสภณ">ชลธี จรัสโสภณ</option>
                    <option value="วันชัย สุรศักดิ์อุดม">วันชัย สุรศักดิ์อุดม</option>
                </select>
            </div>
            <div class="back-sub">
                <label for="namesub">ชื่อรายวิชา</label><br>
                <input type="text"  name="namesub"><br>
                <label for="credit">หน่วยกิต</label><br>
                <input type="text" name="credit"><br>
            </div>
            <div class="save">
                <button id ="btnsave" type="button" name="save">บันทึก</button>
                <button id ="btncancel" type="button">ยกเลิก</button>
            </div>
           </form>
        </div>
    </div>
</body>
</html>