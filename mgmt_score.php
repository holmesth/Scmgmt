<?php 
    session_start(); 
    require_once 'config/db.php';
   
    // if (!isset($_SESSION['admin_login'])) {
    //     $_SESSION['error'] = 'กรุณาเข้าสู่ระบบ!';
    //     header('location: index.php');
    // }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">
    <link rel="stylesheet" href="css/dashboard.css">
    <link rel="stylesheet" href="css/magmt_user.css">
</head>
<body>
    <?php 
        if (isset($_SESSION['admin_login'])) {
            $user_id = $_SESSION['admin_login'];
            $stmt = $conn->query("SELECT * FROM admins WHERE user_id = $user_id");
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            //แสดงชื่อผู้ใช้

        }
    ?>
    <div class="head">
        <div class="hi">
            <div class="img">
                <img src="img/user.png" alt="">
            </div>
            <div class="hello">
                <h3>สวัสดีคุณ</h3>
                <h3>นักเรียน เรียนดี</h3>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="logo">
            <h1>โรงเรียนของหนู</h1>
        </div>

        <ul>
            <li><a href="admin.php"><i id="ic" class="fa-solid fa-house"></i>หน้าหลัก</a></li>
            <li><a href="mgmt_admin.php"><i id="ic"class="fa-solid fa-user"></i>จัดการผู้ใช้งานระบบ</a></li>
            <li><a href="mgmt_sub.php"><i id="ic"class="fa-solid fa-book"></i>จัดการรายวิชา</a></li>
            <li><a href="mgmt_score.php"><i id="ic"class="fa-solid fa-font"></i>จัดการคะแนน</a></li>
            <li><a href="personal_info.php"><i id="ic"class="fa-solid fa-lock"></i>ข้อมูลส่วนตัว</a></li>
            <li class="logout" ><a href="logout.php"><i id="ic"class="fa-solid fa-right-from-bracket"></i>ออกจากระบบ</a></li>
        </ul>
    </div>
    <div class="title">
        <i id="tt"class="fa-solid fa-font"></i>
        <p>จัดการคะแนน</p>
    </div>
    <div class="content">
        <div class="search">
            <form action="" class="search-bar">
                <input type="text" name="search" placeholder="ค้นหา" 
                value="<?php if(isset($_GET['search'])){echo $_GET['search'];}?>">
                <button type="submit"><i class="fa-solid fa-magnifying-glass"></i></button>
            </form>
        </div>
        
        <div class="table" id="sub">
            <div class="topTable">
                <form action="PHP_score.php" class="class-bar" id="tb-bar">
                    <b>ภาคเรียนที่</b>
                    <select name="term-score" >
                        <?php if(!isset($_SESSION['term-score'])){?>
                            <option value=""></option>
                        <?php
                            }else{?>
                                <option value="<?php echo $_SESSION['term-score']; ?>" ><?php echo $_SESSION['term-score'] ?></option>
                        <?php
                            } ?>

                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select>
                    <b>ปีการศึกษา</b>
                    <select name="year-score" >
                        <?php if(!isset($_SESSION['year-score'])){?>
                            <option value=""></option>
                        <?php
                            }else{?>
                                <option value="<?php echo $_SESSION['year-score']; ?>" ><?php echo $_SESSION['year-score'] ?></option>
                        <?php
                            } ?>
                        <?php
                            $yearTable = $conn->query("SELECT DISTINCT year FROM school_year");
                            $yearTable->execute();
                            $years = $yearTable->fetchAll();
                            foreach ($years as $year) {
                        ?>
                                            
                                <option value="<?=  $year['year']; ?>"><?=  $year['year']; ?></option>
                        <?php
                            }
                        ?>
                    </select>
                    <b>ชั้นเรียน</b>
                    <select name="sec-score" >
                        <?php if(!isset($_SESSION['sec-score'])){?>
                            <option value=""></option>
                        <?php
                            }else{?>
                                <option value="<?php echo $_SESSION['sec-score']; ?>" ><?php echo $_SESSION['sec-score'] ?></option>
                        <?php
                            } ?>
                        <?php
                            $clasesTable = $conn->query("SELECT DISTINCT class_name FROM class");
                            $clasesTable->execute();
                            $clases = $clasesTable->fetchAll();
                            foreach ($clases as $class) {
                        ?>
                                            
                                <option value="<?=  $class['class_name']; ?>"><?=  $class['class_name']; ?></option>
                        <?php
                            }
                        ?>
                    </select>

                    <b>วิชา</b>
                    <select name="sub-score" >
                        <?php if(!isset($_SESSION['sub-name'])){?>
                            <option value=""></option>
                        <?php
                            }else{?>
                                <option value="<?php echo $_SESSION['sub-name']; ?>" ><?php echo $_SESSION['sub-name'] ?></option>
                        <?php
                            } ?>
                        <?php
                            $clasesTable = $conn->query("SELECT * FROM subjects");
                            $clasesTable->execute();
                            $clases = $clasesTable->fetchAll();
                            foreach ($clases as $class) {
                        ?>
                                            
                                <option value="<?=  $class['subj_name']; ?>"><?=  $class['subj_name']; ?></option>
                        <?php
                            }
                        ?>
                    </select>
                    <button type="submit" class="s_room" name="sl-score"  value="submit">ค้นหา</button>
            </form>
                <button class="adduser2" type="submit" > <i class="fa-solid fa-circle-plus"></i> เพิ่มรายวิชา</button>
            </div>
            
        

            <div class="table-fix" id="tb-sub">
                <table>
                <tr>
                    <th>#</th>
                    <th>รหัสนักเรียน</th>
                    <th>ชื่อ-นามสกุล</th>
                    <th>คะแนนเก็บ</th>
                    <th>คะแนนกลางภาค</th>
                    <th>คะแนนปลายภาค</th>
                    <th>คะแนนรวม</th>
                    <th>เมนู</th>
                </tr>
                <?php
                     
                    if (!isset($_SESSION['term-score']) and !isset($_SESSION['year-score']) and !isset($_SESSION['sec-score'])and !isset($_SESSION['sub-score'])) {
                        ?>
                        <tr>
                            <td colspan='8' style="text-align: center;">ไม่พบข้อมูล</td>
                        </tr>
                        <?php
                    } else {?><?php
                        if (isset($_GET['search'])) {
                            $filterValues = $_GET['search'];
                            $detailSubjectID =  $_SESSION['dt_sub_id'];
                            $scoreTable = $conn->query("SELECT score.student_id,firstname,lastname, accumulated_score,midterm_score,final_score
                                                        FROM score,students 
                                                        WHERE detail_subject_id = $detailSubjectID
                                                        AND students.student_id = score.student_id
                                                        AND CONCAT(score.student_id,firstname,lastname,accumulated_score,midterm_score,final_score)
                                                        LIKE '%$filterValues%'");
                            $scoreTable->execute();
                            $scores = $scoreTable->fetchAll();
                            $count_user = 0;
                            if (count($scores) > 0) {
                            foreach ($scores as $score) {
                                    $count_user += 1;;
                                    
                        ?>
                                    <tr>
                                        <td><?= $count_user; ?></td>
                                        <td><?= $score['student_id']; ?></td>
                                        <td><?= $score['firstname'] . ' ' . $score['lastname']; ?></td>
                                        <td><?= $score['accumulated_score']; ?></td>
                                        <td><?= $score['midterm_score']; ?></td>
                                        <td><?= $score['final_score']; ?></td>
                                        <td><?= (int)$score['accumulated_score'] + (int)$score['midterm_score'] +(int)$score['final_score'] ?></td>
                                        <td><?= $count_user; ?></td>
                                    </tr>
                                <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan='6' style="text-align: center;">ไม่พบข้อมูล</td>
                                </tr>
                            <?php
                            }
                        } else {
                            $detailSubjectID =  $_SESSION['dt_sub_id'];
                            $scoreTable = $conn->query("SELECT score.student_id,firstname,lastname, accumulated_score,midterm_score,final_score
                                                        FROM score,students 
                                                        WHERE detail_subject_id = $detailSubjectID
                                                        AND students.student_id = score.student_id;");
                            $scoreTable->execute();
                            $scores = $scoreTable->fetchAll();
                            
                            $count_user = 0;
                            
                            foreach ($scores as $score) { 
                                $count_user += 1;
                            ?>
                                <tr >
                                    <!-- เอาข้อมูลมาโชว์ที่ตาราง -->
                                    <td><?= $count_user; ?></td>
                                    <td><?= $score['student_id']; ?></td>
                                    <td><?= $score['firstname'] . ' ' . $score['lastname']; ?></td>
                                    <td><?= $score['accumulated_score']; ?></td>
                                    <td><?= $score['midterm_score']; ?></td>
                                    <td><?= $score['final_score']; ?></td>
                                    <td><?= (int)$score['accumulated_score'] + (int)$score['midterm_score'] +(int)$score['final_score'] ?></td>
                                    <td><?= $count_user; ?></td>
                                </tr>
                        <?php
                            }
                        }?> 
                  <?php  }
                    
                ?>
                </table>
            </div>
        </div>
    </div>
</body>
</html>