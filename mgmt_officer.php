<?php 
    session_start(); 
    require_once 'config/db.php';
    // if (!isset($_SESSION['admin_login'])) {
    //     $_SESSION['error'] = 'กรุณาเข้าสู่ระบบ!';
    //     header('location: index.php');
    // }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">
    <link rel="stylesheet" href="css/dashboard.css">
    <link rel="stylesheet" href="css/magmt_user.css">
</head>
<body>
    <?php 
        if (isset($_SESSION['admin_login'])) {
            $user_id = $_SESSION['admin_login'];
            $stmt = $conn->query("SELECT * FROM admins WHERE user_id = $user_id");
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            //แสดงชื่อผู้ใช้

        }
    ?>
    <div class="head">
        <div class="hi">
            <div class="img">
                <img src="img/user.png" alt="">
            </div>
            <div class="hello">
                <h3>สวัสดีคุณ</h3>
                <h3>นักเรียน เรียนดี</h3>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="logo">
            <h1>โรงเรียนของหนู</h1>
        </div>

        <ul>
            <li><a href="admin.php"><i id="ic" class="fa-solid fa-house"></i>หน้าหลัก</a></li>
            <li><a href="mgmt_admin.php"><i id="ic"class="fa-solid fa-user"></i>จัดการผู้ใช้งานระบบ</a></li>
            <li><a href="mgmt_sub.php"><i id="ic"class="fa-solid fa-book"></i>จัดการรายวิชา</a></li>
            <li><a href="mgmt_score.php"><i id="ic"class="fa-solid fa-font"></i>จัดการคะแนน</a></li>
            <li><a href="personal_info.php"><i id="ic"class="fa-solid fa-lock"></i>ข้อมูลส่วนตัว</a></li>
            <li class="logout" ><a href="logout.php"><i id="ic"class="fa-solid fa-right-from-bracket"></i>ออกจากระบบ</a></li>
        </ul>
    </div>
    <div class="title">
        <i id="tt"class="fa-solid fa-user"></i>
        <p>จัดการผู้ใช้งานระบบ</p>
    </div>
    <div class="content">
        <div class="search">
            <form action="" class="search-bar">
                <input type="text" name="search" placeholder="ค้นหา" 
                value="<?php if(isset($_GET['search'])){echo $_GET['search'];}?>">
                <button type="submit"><i class="fa-solid fa-magnifying-glass"></i></button>
            </form>
        </div>
        <div class="all_user">
            <div class="list_user">
                <a href="mgmt_admin.php">
                    <div class="user_admin" >
                        <i class="fa-solid fa-user"></i>
                        <h3>แอดมิน</h3>
                    </div>
                </a>
                <a href="mgmt_officer.php">
                    <div class="user_admin" id="officer">
                        <i class="fa-solid fa-laptop"></i>
                        <h3>เจ้าหน้าที่</h3>
                    </div>
                </a>
                <a href="mgmt_teacher.php">
                    <div class="user_admin">
                        <i class="fa-solid fa-chalkboard-user"></i>
                        <h3>อาจารย์</h3>
                    </div>
                </a>
                <a href="mgmt_student.php">
                    <div class="user_admin">
                        <i class="fa-solid fa-graduation-cap"></i>
                        <h3>นักเรียน</h3>
                    </div>
                </a>
            </div>
        </div>
        <div class="table">
            <div class="topTable">
                <a href="add_officer.php">
                    <button class="adduser2" type="submit" > <i class="fa-solid fa-circle-plus"></i> เพิ่มเจ้าหน้าที่</button>
                </a>
            </div>
            <div class="table-fix">
                <table>
                <tr>
                    <th>#</th>
                    <th>ชื่อ-นามสกุล</th>
                    <th>เบอร์โทรศัพท์</th>
                    <th>อีเมล</th>
                    <th>ตำแหน่ง</th>
                    <th>เมนู</th>
                </tr>
                <?php
                    if (isset($_GET['search'])) {
                        $filterValues = $_GET['search'];
                        $officerTable = $conn->query("SELECT * FROM officers,users 
                                                    WHERE officers.user_id = users.user_id 
                                                    AND CONCAT(officer_id,firstname,lastname,position,tel_number,email) 
                                                    LIKE '%$filterValues%';");
                        $officerTable->execute();
                        $officers = $officerTable->fetchAll();
                        if (count($officers) > 0) {
                            $count_user = 0;
                            foreach ($officers as $officer) {
                            $count_user += 1; 
                    ?>
                                <tr>
                                    <td><?= $count_user; ?></td>
                                    <td><?= $officer['firstname'] . ' ' . $officer['lastname']; ?></td>
                                    <td><?= $officer['tel_number']; ?></td>
                                    <td><?= $officer['email']; ?></td>
                                    <td><?= $officer['position']; ?></td>
                                    <td><?= $count_user; ?></td>
                                </tr>
                            <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan='6' style="text-align: center;">ไม่พบข้อมูล</td>
                            </tr>
                        <?php
                        }
                    } else {
                        $officerTable = $conn->query("SELECT * FROM officers,users WHERE officers.user_id = users.user_id;");
                        $officerTable->execute();
                        $officers = $officerTable->fetchAll();
                        $count_user = 0;
                        foreach ($officers as $officer) {
                            $count_user += 1;
                        ?>
                            <tr>
                                <!-- เอาข้อมูลมาโชว์ที่ตาราง -->
                                <td><?= $count_user; ?></td>
                                <td><?= $officer['firstname'] . ' ' . $officer['lastname']; ?></td>
                                <td><?= $officer['tel_number']; ?></td>
                                <td><?= $officer['email']; ?></td>
                                <td><?= $officer['position']; ?></td>
                                <td><?= $count_user; ?></td>
                            </tr>
                    <?php
                        }
                    }?> 
                </table>
            </div>
        </div>
    </div>
</body>
</html>