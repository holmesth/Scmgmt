<script src="https://code.jquery.com/jquery-3.6.3.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<?php
    session_start();
    require_once 'config/db.php';

    if (isset($_POST['btnsave'])){
        $name = $_POST['fname'];
        $email = $_POST['email'];
        $tell = $_POST['tell'];
        $position = $_POST['position'];
        $password = $_POST['password'];
        $confirm = $_POST['cfpassword'];
        $full_name = explode(" ", $name);
        $firstname = $full_name[0];
        $lastname = $full_name[1];
        $urole = "admin";
        $targetDir = "img/";
        
        
        if (!empty($_FILES["file"]["name"])) {
            $est = explode(".", $_FILES["file"]["name"]);
            $fileName = md5(uniqid()).".{$est[1]}";
        }else{
            $fileName = "user.png";
        }

        $targetFilePath = $targetDir . $fileName;
        

        if (empty($name)){
            $_SESSION['warning'] = 'กรุณากรอกชื่อ-นามสกุล';
            header("location: add_admin.php");
        }else if (empty($email)) {
            $_SESSION['warning'] = 'กรุณากรอกอีเมล';
            header("location: add_admin.php");
        } else if (empty($tell)) {
            $_SESSION['warning'] = 'กรุณากรอกเบอร์โทร';
            header("location: add_admin.php");
        }else if (empty($position)) {
            $_SESSION['warning'] = 'กรุณากรอกตำแหน่ง';
            header("location: add_admin.php");
        }else if (empty($password)) {
            $_SESSION['warning'] = 'กรุณากรอกรหัสผ่าน';
            header("location: add_admin.php");
        }else if (empty($confirm)) {
            $_SESSION['warning'] = 'กรุณากรอกยืนยันรหัสผ่าน';
            header("location: add_admin.php");
        }else if ($confirm != $password) {
            $_SESSION['warning'] = 'ยืนยันรหัสผ่านไม่ถูกต้อง';
            header("location: add_admin.php");
        }else if (empty($lastname)) {
            $_SESSION['warning'] = 'กรอกชื่อไม่ถูกต้อง';
            header("location: add_admin.php");
        }else{
            try{
                
                $check_email = $conn->prepare("SELECT email FROM users WHERE email = :email");
                $check_email->bindParam(":email", $email);
                $check_email->execute();
                $row = $check_email->fetch(PDO::FETCH_ASSOC);

                if(!empty($row['email'])){
                    if($row['email'] == $email){
                        $_SESSION['warning'] = 'อีเมลนี้มีคนใช้แล้ว';
                        header("location: add_admin.php");
                    }
                }else if(!isset($_SESSION['warning'])){
                    
                    move_uploaded_file($_FILES['file']['tmp_name'], $targetFilePath);
        
                    $passwordHash = password_hash($password, PASSWORD_DEFAULT);
                    $insert_stmt_user = $conn->prepare("INSERT INTO users(email, password ,urole) 
                                                    VALUE(:email, :password ,:urole)");
                    $insert_stmt_user->bindParam(":email", $email);
                    $insert_stmt_user->bindParam(":password", $passwordHash);
                    $insert_stmt_user->bindParam(":urole", $urole);
                    $insert_stmt_user->execute();
                    

                    $take_id = $conn->prepare("SELECT user_id FROM users WHERE email = :email");
                    $take_id->bindParam(":email", $email);
                    $take_id->execute();
                    $row_id = $take_id->fetch(PDO::FETCH_ASSOC);

                    


                    $insert_stmt_admin = $conn->prepare("INSERT INTO admins(firstname, lastname, position, tel_number, img, user_id) 
                                                    VALUE(:firstname, :lastname ,:position, :tel_number, :img, :user_id)");
                    $insert_stmt_admin->bindParam(":firstname", $firstname);
                    $insert_stmt_admin->bindParam(":lastname", $lastname);
                    $insert_stmt_admin->bindParam(":position", $position);
                    $insert_stmt_admin->bindParam(":tel_number", $tell);
                    $insert_stmt_admin->bindParam(":img", $fileName);
                    $insert_stmt_admin->bindParam(":user_id", $row_id['user_id']);
                    $insert_stmt_admin->execute();

                    echo "<script>
                        $(document).ready(function() {
                            Swal.fire({
                                title: 'success',
                                text: 'เพิ่มข้อมูลแอดมินเรียบร้อย  ',
                                icon: 'success',
                                timer: 5000,
                                showConfirmButton: false
                            });
                        })
                        </script>";
                    header("refresh:2; url=mgmt_admin.php");
                }else{
                    $_SESSION['warning'] = 'มีบางอย่างผิดพลาด';
                    header("location: add_admin.php");
                }
            }catch(PDOException $e) {
                echo $e->getMessage();
            }
        }
        
    }

    
?>