<?php 

    session_start();
    unset($_SESSION['admin_login']);
    unset($_SESSION['officer_login']);
    unset($_SESSION['teacher_login']);
    unset($_SESSION['student_login']);
    unset($_SESSION['term']);
    unset($_SESSION['year']);
    unset($_SESSION['sec']);
    unset($_SESSION['term-sub']);
    unset($_SESSION['year-sub']);
    unset($_SESSION['sec-sub']);
    unset($_SESSION['year-score']);
    unset($_SESSION['term-score']);
    unset($_SESSION['sec-score']);
    unset($_SESSION['sub-score']);
    unset($_SESSION['sub-name']);
    
    
    header('location: index.php');

?>