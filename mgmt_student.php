<?php 
    session_start();
    require_once 'config/db.php'; 
    
    // echo $_SESSION['sec'];
    // echo $_SESSION['term'];
    // echo $_SESSION['year'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">
    <link rel="stylesheet" href="css/dashboard.css">
    <link rel="stylesheet" href="css/magmt_user.css">
</head>
<body>
    <?php 
        if (isset($_SESSION['admin_login'])) {
            $user_id = $_SESSION['admin_login'];
            $stmt = $conn->query("SELECT * FROM admins WHERE user_id = $user_id");
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            //แสดงชื่อผู้ใช้

        }
    ?>
    <div class="head">
        <div class="hi">
            <div class="img">
                <img src="img/user.png" alt="">
            </div>
            <div class="hello">
                <h3>สวัสดีคุณ</h3>
                <h3>นักเรียน เรียนดี</h3>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="logo">
            <h1>โรงเรียนของหนู</h1>
        </div>

        <ul>
            <li><a href="admin.php"><i id="ic" class="fa-solid fa-house"></i>หน้าหลัก</a></li>
            <li><a href="mgmt_admin.php"><i id="ic"class="fa-solid fa-user"></i>จัดการผู้ใช้งานระบบ</a></li>
            <li><a href="mgmt_sub.php"><i id="ic"class="fa-solid fa-book"></i>จัดการรายวิชา</a></li>
            <li><a href="mgmt_score.php"><i id="ic"class="fa-solid fa-font"></i>จัดการคะแนน</a></li>
            <li><a href="personal_info.php"><i id="ic"class="fa-solid fa-lock"></i>ข้อมูลส่วนตัว</a></li>
            <li class="logout" ><a href="logout.php"><i id="ic"class="fa-solid fa-right-from-bracket"></i>ออกจากระบบ</a></li>
        </ul>
    </div>
    <div class="title">
        <i id="tt"class="fa-solid fa-user"></i>
        <p>จัดการผู้ใช้งานระบบ</p>
    </div>
    <div class="content">
        <div class="search">
            <form action="" class="search-bar">
                <input type="text" name="search" placeholder="ค้นหา" 
                value="<?php if(isset($_GET['search'])){echo $_GET['search'];}?>">
                <button type="submit"><i class="fa-solid fa-magnifying-glass"></i></button>
            </form>
        </div>
        <div class="all_user">
            <div class="list_user">
                <a href="mgmt_admin.php">
                    <div class="user_admin" >
                        <i class="fa-solid fa-user"></i>
                        <h3>แอดมิน</h3>
                    </div>
                </a>
                <a href="mgmt_officer.php">
                    <div class="user_admin" >
                        <i class="fa-solid fa-laptop"></i>
                        <h3>เจ้าหน้าที่</h3>
                    </div>
                </a>
                <a href="mgmt_teacher.php">
                    <div class="user_admin" >
                        <i class="fa-solid fa-chalkboard-user"></i>
                        <h3>อาจารย์</h3>
                    </div>
                </a>
                <a href="mgmt_student.php">
                    <div class="user_admin" id="student">
                        <i class="fa-solid fa-graduation-cap"></i>
                        <h3>นักเรียน</h3>
                    </div>
                </a>
            </div>
        </div>
       
        <div class="table">
            <div class="topTable">
                <form action="PHP_search.php" class="class-bar" >
                    <b>ภาคเรียนที่</b>
                    <select name="term" >
                        <?php if(!isset($_SESSION['term'])){?>
                            <option value=""></option>
                        <?php
                            }else{?>
                                <option value="<?php echo $_SESSION['term']; ?>" ><?php echo $_SESSION['term'] ?></option>
                        <?php
                            } ?>

                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select>
                    <b>ปีการศึกษา</b>
                    <select name="year" >
                        <?php if(!isset($_SESSION['year'])){?>
                            <option value=""></option>
                        <?php
                            }else{?>
                                <option value="<?php echo $_SESSION['year']; ?>" ><?php echo $_SESSION['year'] ?></option>
                        <?php
                            } ?>
                        <?php
                            $yearTable = $conn->query("SELECT DISTINCT year FROM school_year");
                            $yearTable->execute();
                            $years = $yearTable->fetchAll();
                            foreach ($years as $year) {
                        ?>
                                            
                                <option value="<?=  $year['year']; ?>"><?=  $year['year']; ?></option>
                        <?php
                            }
                        ?>
                    </select>
                    <b>ชั้นเรียน</b>
                    <select name="sec" >
                        <?php if(!isset($_SESSION['sec'])){?>
                            <option value=""></option>
                        <?php
                            }else{?>
                                <option value="<?php echo $_SESSION['sec']; ?>" ><?php echo $_SESSION['sec'] ?></option>
                        <?php
                            } ?>
                        <?php
                            $clasesTable = $conn->query("SELECT DISTINCT class_name FROM class");
                            $clasesTable->execute();
                            $clases = $clasesTable->fetchAll();
                            foreach ($clases as $class) {
                        ?>
                                            
                                <option value="<?=  $class['class_name']; ?>"><?=  $class['class_name']; ?></option>
                        <?php
                            }
                        ?>
                </select>
                <button type="submit" class="s_room" name="sl-stu" value="submit">ค้นหา</button>
            </form>
                <a href="add_student.php">
                    <button class="adduser2" type="submit" > <i class="fa-solid fa-circle-plus"></i> เพิ่มนักเรียน</button>
                </a>
            </div>
            
            
            <?php
               
                if(isset($_SESSION['year']) and isset($_SESSION['sec']) and isset($_SESSION['term'])){
                    $school_yaer = $_SESSION['year'];
                    $school_term = $_SESSION['term']; 
                    $room = substr($_SESSION['sec'],-1);
                    $class_id = $room . $school_yaer . $school_term;
                }else{
                    $class_id = '';
                }
                
                
            ?>
            
            <div class="table-fix">
                <table>
                <tr>
                    <th>รหัสนักเรียน</th>
                    <th>ชื่อ-นามสกุล</th>
                    <th>เบอร์โทรศัพท์</th>
                    <th>อีเมล</th>
                    <th>เมนู</th>
                </tr>
                <?php
                    if ($class_id == '') {
                        ?>
                        <tr>
                            <td colspan='6' style="text-align: center;">ไม่พบข้อมูล</td>
                        </tr>
                        <?php
                    } else {?><?php
                        if (isset($_GET['search'])) {
                            $filterValues = $_GET['search'];
                            $studentTable = $conn->query
                            ("SELECT students.student_id,email,firstname,lastname,tel_number 
                            FROM detail_class,users,students 
                            WHERE class_id = '$class_id' 
                            AND students.user_id = users.user_id 
                            AND detail_class.student_id = students.student_id 
                            AND CONCAT(detail_class_id,firstname,lastname,tel_number,email)
                            LIKE '%$filterValues%'");
                            $studentTable->execute();
                            $students = $studentTable->fetchAll();
                            
                            if (count($students) > 0) {
                                foreach ($students as $student) {
                                    
                        ?>
                                    <tr>
                                        <td><?= $student['student_id']; ?></td>
                                        <td><?= $student['firstname'] . ' ' . $student['lastname']; ?></td>
                                        <td><?= $student['tel_number']; ?></td>
                                        <td><?= $student['email']; ?></td>
                                        <td><?= $student['student_id']; ?></td>
                                    </tr>
                                <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan='6' style="text-align: center;">ไม่พบข้อมูล</td>
                                </tr>
                            <?php
                            }
                        } else {
                            $studentTable = $conn->query
                            ("SELECT students.student_id,email,firstname,lastname,tel_number
                            FROM detail_class,users,students WHERE class_id = $class_id 
                            AND students.user_id = users.user_id 
                            AND detail_class.student_id = students.student_id");
                            $studentTable->execute();
                            $students = $studentTable->fetchAll();
                            $count_user = 0;
                            foreach ($students as $student) {
                            $count_user += 1;
                            ?>
                                <tr>
                                    <!-- เอาข้อมูลมาโชว์ที่ตาราง -->
                                    <td><?= $student['student_id']; ?></td>
                                    <td><?= $student['firstname'] . ' ' . $student['lastname']; ?></td>
                                    <td><?= $student['tel_number']; ?></td>
                                    <td><?= $student['email']; ?></td>
                                    <td><?= $student['student_id']; ?></td>
                                </tr>
                        <?php
                            }
                        }?> 
                  <?php  }
                    
                ?>
                </table>
            </div>
        </div>
    </div>
</body>
</html>