<?php 
    session_start(); 
    require_once 'config/db.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>เข้าสู่ระบบ</title>
    <link rel="stylesheet" href="css/login.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <form action="PHP_signin_db.php" method="post">
        <div class="login">
            <div class="logo">
                <img src="img/user.png" alt="">
                <h1>โรงเรียนของหนู</h1>
                
            </div>
            <div class="error">
                    <?php if(isset($_SESSION['error'])) { ?>
                        <div class="alert alert-danger" role="alert">
                            <?php 
                                echo $_SESSION['error'];
                                unset($_SESSION['error']);
                            ?>
                        </div>
                    <?php } ?>
                    <?php if(isset($_SESSION['success'])) { ?>
                        <div class="alert alert-success" role="alert">
                            <?php 
                                echo $_SESSION['success'];
                                unset($_SESSION['success']);
                            ?>
                        </div>
                    <?php } ?>
                </div>
            <div class="input">
                <input type="email" name="email" placeholder="อีเมล">
                <input type="password" name="password" placeholder="รหัสผ่าน">
            </div>
            <div class="button">
                <button type="submit" name="signin">เข้าสู่ระบบ</button>
            </div>
        </div>
    </form>
    
</body>
</html>