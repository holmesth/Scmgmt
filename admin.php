<?php 

    session_start();
    require_once 'config/db.php';
    if (!isset($_SESSION['admin_login'])) {
        $_SESSION['error'] = 'กรุณาเข้าสู่ระบบ!';
        header('location: index.php');
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>หน้าแรก</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">
    <link rel="stylesheet" href="css/dashboard.css">
    <link rel="stylesheet" href="css/admin.css">
</head>
<body>
    <?php 
        if (isset($_SESSION['admin_login'])) {
            $user_id = $_SESSION['admin_login'];
            $stmt = $conn->query("SELECT * FROM admins WHERE user_id = $user_id");
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $rowOfficer = $conn->query("SELECT * FROM officers")->fetchAll();
            $rowStudent = $conn->query("SELECT * FROM students")->fetchAll();
            $rowTeacher = $conn->query("SELECT * FROM teachers")->fetchAll();
            // echo count($rowOfficer);
        }
    ?>
    <div class="head">
        <div class="hi">
            <div class="img">
                <img src="img/user.png" alt="">
            </div>
            <div class="hello">
                <h3>สวัสดีคุณ</h3>
                <h3><?php echo $row['firstname'] . ' ' . $row['lastname'] ?></h3>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="logo">
            <h1>โรงเรียนของหนู</h1>
        </div>

        <ul>
            <li><a href="admin.php"><i id="ic" class="fa-solid fa-house"></i>หน้าหลัก</a></li>
            <li><a href="mgmt_admin.php"><i id="ic"class="fa-solid fa-user"></i>จัดการผู้ใช้งานระบบ</a></li>
            <li><a href="mgmt_sub.php"><i id="ic"class="fa-solid fa-book"></i>จัดการรายวิชา</a></li>
            <li><a href="mgmt_score.php"><i id="ic"class="fa-solid fa-font"></i>จัดการคะแนน</a></li>
            <li><a href="personal_info.php"><i id="ic"class="fa-solid fa-lock"></i>ข้อมูลส่วนตัว</a></li>
            <li class="logout" ><a href="logout.php"><i id="ic"class="fa-solid fa-right-from-bracket"></i>ออกจากระบบ</a></li>
        </ul>
    </div>
    <div class="title">
        <i id="tt" class="fa-solid fa-house"></i>
        <p>หน้าหลัก</p>
    </div>
    <div class="content">
        <div class="welcome">
            <img src="https://images.unsplash.com/photo-1595206133361-b1fe343e5e23?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="">
        </div>
        <div class="count_user">
            <div class="officer">
                <i id="gg" class="fa-solid fa-laptop"></i>
                <h3><?php echo 'เจ้าหน้าที่ '.count($rowOfficer). ' คน' ?></h3>
            </div>
            <div class="teacher">
                <i id="gg" class="fa-solid fa-chalkboard-user"></i>
                <h3><?php echo 'คุณครู '.count($rowTeacher). ' คน' ?></h3>
            </div>
            <div class="student">
                <i id="gg" class="fa-solid fa-graduation-cap"></i>
                <h3><?php echo 'นักเรียน '.count($rowStudent). ' คน' ?></h3>
            </div>
        </div>
    </div>
</body>
</html>