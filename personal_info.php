<?php 
    session_start(); 
    require_once 'config/db.php';
    if (!isset($_SESSION['admin_login'])) {
        $_SESSION['error'] = 'กรุณาเข้าสู่ระบบ!';
        header('location: index.php');
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">
    <link rel="stylesheet" href="css/dashboard.css">
    <link rel="stylesheet" href="css/input.css">
</head>
<body>
    <?php 
        if (isset($_SESSION['admin_login'])) {
            $user_id = $_SESSION['admin_login'];
            $stmt = $conn->query("SELECT * FROM admins ,users WHERE users.user_id = $user_id AND admins.admin_id =$user_id");
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
        }
    ?>
    <div class="head">
        <div class="hi">
            <div class="img">
                <img src="img/user.png" alt="">
            </div>
            <div class="hello">
                <h3>สวัสดีคุณ</h3>
                <h3><?php echo $row['firstname'] . ' ' . $row['lastname'] ?></h4>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="logo">
            <h1>โรงเรียนของหนู</h1>
        </div>

        <ul>
            <li><a href="admin.php"><i id="ic" class="fa-solid fa-house"></i>หน้าหลัก</a></li>
            <li><a href="mgmt_admin.php"><i id="ic"class="fa-solid fa-user"></i>จัดการผู้ใช้งานระบบ</a></li>
            <li><a href="mgmt_sub.php"><i id="ic"class="fa-solid fa-book"></i>จัดการรายวิชา</a></li>
            <li><a href="mgmt_score.php"><i id="ic"class="fa-solid fa-font"></i>จัดการคะแนน</a></li>
            <li><a href="personal_info.php"><i id="ic"class="fa-solid fa-lock"></i>ข้อมูลส่วนตัว</a></li>
            <li class="logout" ><a href="logout.php"><i id="ic"class="fa-solid fa-right-from-bracket"></i>ออกจากระบบ</a></li>
        </ul>
    </div>
    <div class="title">
        <i id="tt" class="fa-solid fa-lock"></i>
        <p>ข้อมูลส่วนตัว</p>
    </div>
    <div class="content">
        <div class="input">
            <div class="img_data">
                <img src="img_girl.jpg" alt="" width="120" height="120"><br>
            </div>
            <div class="front" id="info_ps">
                <label for="fname">ชื่อ-นามสกุล </label><br>
                <input type="text"  name="name" disabled="disabled" 
                    placeholder="<?php echo $row['firstname'] . ' ' . $row['lastname'] ?>"><br>
                <label for="tell">เบอร์โทร </label><br>
                <input type="text"  name="tell" disabled="disabled"
                    placeholder="<?php echo $row['tel_number'] ?>"><br>
                <label for="password">รหัสผ่าน </label><br>
                <input type="password"  name="password" disabled="disabled"
                    placeholder="*********">
            </div>
            <div class="back" id="info_ps">
                <label for="lname" >อีเมล </label><br>
                <input type="email"  name="email" disabled="disabled"
                    placeholder="<?php echo $row['email'] ?>"><br>
                <label for="position">ตำแหน่ง </label><br>
                <input type="text" name="position" disabled="disabled"
                    placeholder="<?php echo $row['position'] ?>"><br>
            </div>
            <div class="chang_password">
                <button id ="btn_chang_pwd" type="button"><i id="ic" class="fa-solid fa-pen"></i>   เปลี่ยนรหัสผ่าน</button>
            </div>
        </div>
    </div>
</body>
</html>